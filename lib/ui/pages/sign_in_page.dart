part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    Widget bgColor() {
      return Container(
        width: double.infinity,
        height: double.infinity,
        color: orangeColor.withOpacity(0.8),
      );
    }

    Widget logo() {
      return Container(
        width: 95,
        height: 95,
        margin: const EdgeInsets.only(top: 88, bottom: 13),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/logo.png'), fit: BoxFit.cover)),
      );
    }

    Widget title() {
      return Text(
        'Let’s Sign You In',
        style: blackFontStyle1.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      );
    }

    Widget subtitle() {
      return Text(
        'Welcome back, you’ve been missed!',
        style: blackFontStyle1.copyWith(
          fontSize: 13,
          fontWeight: FontWeight.w400,
          color: Colors.white,
        ),
      );
    }

    Widget inputEmail() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
            child: Text(
              'Email Adress',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: emailController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: greyFontStyle,
                hintText: 'Type your email address',
              ),
            ),
          ),
        ],
      );
    }

    Widget inputPassword() {
      return Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'Password',
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              obscureText: true,
              controller: passwordController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: greyFontStyle,
                hintText: 'Type your password',
              ),
            ),
          ),
        ],
      );
    }

    Widget signInButton() {
      return Container(
        width: double.infinity,
        height: 45,
        margin: EdgeInsets.only(top: defaultMargin, bottom: 16),
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: isLoading
            ? loadingIndicator
            : RaisedButton(
                onPressed: () async {
                  setState(() {
                    isLoading = true;
                  });

                  await context
                      .bloc<UserCubit>()
                      .signIn(emailController.text, passwordController.text);
                  UserState state = context.bloc<UserCubit>().state;

                  if (state is UserLoaded) {
                    context.bloc<FoodCubit>().getFood();
                    context.bloc<TransactionCubit>().getTransactions();
                    Get.to(MainPage());
                  } else {
                    Get.snackbar(
                      '',
                      '',
                      backgroundColor: 'D9435E'.toColor(),
                      icon: Icon(
                        MdiIcons.closeCircleOutline,
                        color: Colors.white,
                      ),
                      titleText: Text(
                        'Sign In Failed',
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      messageText: Text(
                        (state as UserLoadingFailed).message,
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                        ),
                      ),
                    );
                    setState(() {
                      isLoading = false;
                    });
                  }
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
                color: orangeColor,
                child: Text(
                  'Sign In',
                  style: blackFontStyle3.copyWith(color: Colors.white),
                ),
              ),
      );
    }

    Widget createAccountButton() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Don\'t have an account?',
            style: blackFontStyle3.copyWith(
                fontSize: 15, fontWeight: FontWeight.w300),
          ),
          TextButton(
            onPressed: () {
              Get.to(SignUpPage());
            },
            child: Text(
              'Sign Up',
              style: blackFontStyle3.copyWith(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                  color: orangeColor),
            ),
          )
        ],
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          bgColor(),
          SingleChildScrollView(
            child: Center(
              child: Column(
                children: [
                  logo(),
                  title(),
                  const SizedBox(
                    height: 10,
                  ),
                  subtitle(),
                  const SizedBox(
                    height: 56,
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: 14),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 34),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(33),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        inputEmail(),
                        inputPassword(),
                        signInButton(),
                        createAccountButton(),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
