part of 'pages.dart';

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  int selectedIndex =
      0; // *selectedIndex ini buat menentukan index dari item yang dipilih di mulai dari angka 0

  @override
  Widget build(BuildContext context) {
    double listItemWidht =
        MediaQuery.of(context).size.width - 2 * defaultMargin;

    Widget header() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        color: mainColor,
        height: 100,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Lapar~~~',
                  style: blackFontStyle1.copyWith(color: Colors.white),
                ),
                Text(
                  "What do you want to eat?",
                  style: greyFontStyle.copyWith(
                      fontWeight: FontWeight.w300, color: Colors.white),
                ),
              ],
            ),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                  image: NetworkImage(
                      (context.bloc<UserCubit>().state as UserLoaded)
                          .user
                          .picturePath),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget cardOfFood() {
      return Container(
        height: 258,
        width: double.infinity,
        child: BlocBuilder<FoodCubit, FoodState>(
          builder: (_, state) => (state is FoodLoaded)
              ? ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Row(
                      children: state.foods
                          .map((e) => Padding(
                                padding: EdgeInsets.only(
                                  left: (e == state.foods.first)
                                      ? defaultMargin
                                      : 0,
                                  right: defaultMargin,
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    Get.to(FoodDetailPage(
                                      transaction: Transaction(
                                          food: e,
                                          user: (context.bloc<UserCubit>().state
                                                  as UserLoaded)
                                              .user),
                                      onBackButtonPressed: () {
                                        Get.back();
                                      },
                                    ));
                                  },
                                  child: FoodCard(e),
                                ),
                              ))
                          .toList(),
                    )
                  ],
                )
              : Center(child: loadingIndicator),
        ),
      );
    }

    Widget tabBar() {
      return Container(
        width: double.infinity,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTabBar(
              selectedIndex: selectedIndex,
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
            SizedBox(
              height: 40,
            ),
            BlocBuilder<FoodCubit, FoodState>(
              builder: (_, state) {
                if (state is FoodLoaded) {
                  List<Food> foods = state.foods
                      .where((element) =>
                          element.types.contains((selectedIndex == 0)
                              ? FoodType.new_food
                              : (selectedIndex == 1)
                                  ? FoodType.popular
                                  : FoodType.recomended))
                      .toList();
                  return Column(
                    children: foods
                        .map((e) => Padding(
                              padding: EdgeInsets.fromLTRB(
                                  defaultMargin, 0, defaultMargin, 16),
                              child: FoodListItem(
                                  food: e, itemWidht: listItemWidht),
                            ))
                        .toList(),
                  );
                } else {
                  return Center(
                    child: loadingIndicator,
                  );
                }
              },
            ),
            SizedBox(
              height: 80,
            ),
          ],
        ),
      );
    }

    return ListView(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 30, 0, 10),
              child: Text(
                'Today’s promo',
                style: blackFontStyle1,
              ),
            ),
            cardOfFood(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              height: 164,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/campain.png'),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            tabBar(),
          ],
        ),
      ],
    );
  }
}
