part of 'pages.dart';

class PaymentMethodage extends StatelessWidget {
  final String paymentUrl;

  PaymentMethodage(this.paymentUrl);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IlustrationPage(
          title: 'Finish Your Payment',
          subtitle: 'Please selecet your favorite \npayment method',
          picturePath: 'assets/Payment.png',
          buttonTitle1: 'Payment Method',
          buttonTap1: () async {
            await launch(paymentUrl);
          },
          buttonTitle2: 'Continue ',
          buttonTap2: () {
            Get.to(SuccessOrderPage());
          }),
    );
  }
}
