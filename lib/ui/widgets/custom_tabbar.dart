part of 'widgets.dart';

class CustomTabBar extends StatelessWidget {
  final int selectedIndex;
  final List<String> titles;

  final Function(int) onTap;

  CustomTabBar({this.selectedIndex, @required this.titles, this.onTap});
  @override
  Widget build(BuildContext context) {
    Widget homeButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(0);
          }
        },
        child: Column(
          children: [
            Container(
              width: 75,
              height: 75,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(
                      'assets/ic_favorit' +
                          ((selectedIndex == 0) ? '.png' : '_normal.png'),
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            Text('Favorit', style: blackFontStyle2.copyWith(fontSize: 14)),
          ],
        ),
      );
    }

    Widget orderButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(1);
          }
        },
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 83),
              width: 74,
              height: 74,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(
                      'assets/ic_cheap' +
                          ((selectedIndex == 1) ? '.png' : '_normal.png'),
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            Text('Cheap', style: blackFontStyle2.copyWith(fontSize: 14)),
          ],
        ),
      );
    }

    Widget profileButton() {
      return GestureDetector(
        onTap: () {
          if (onTap != null) {
            onTap(2);
          }
        },
        child: Column(
          children: [
            Container(
              width: 74,
              height: 74,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: AssetImage(
                      'assets/ic_trend' +
                          ((selectedIndex == 2) ? '.png' : '_normal.png'),
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            Text('Trend', style: blackFontStyle2.copyWith(fontSize: 14)),
          ],
        ),
      );
    }

    return Container(
      height: 120,
      width: double.infinity,
      color: Colors.white,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            homeButton(),
            orderButton(),
            profileButton(),
          ],
        ),
      ),
    );
  }
}
